# SABD Project 1 #

Repository per il progetto 1 per l'esame di Sistemi e Architetture per Big Data (SABD).
Usando Apache Spark si processano i dati salvati all'interno di un file .csv. I dati rappresentano i consumi energetici di prese intelligenti.

E' stato richiesto di risolvere le seguenti queries:

* Individuare le case con consumo di potenza istantaneo maggiore o uguale a 350 Watt.
* Per ogni casa, calcolare il consumo energetico medio e la sua deviazione standard nelle quattro fasce orarie: notte, dalle ore 00:00 alle ore 05:59; mattino, dalle 06:00 alle 11:59; pomeriggio, dalle 12:00 alle 17:59; e sera, dalle 18:00 alle 23:59. Il consumo energetico di una casa e dato dalla somma dei consumi energetici di ogni presa intelligente collocata nella casa.
* Si considerino le seguenti fasce di consumo dell’energia elettrica, differenziate in base all’ora e al giorno della settimana, ed a cui corrispondono differenti tariffe: fascia di punta, che si applica negli orari diurni dal lunedi al venerdi (dalle 06:00 alle 17:59) ed a cui corrisponde una tariffa piu alta, e fascia fuori punta, che si applica negli orari notturni dal luned`ı al venerd`ı (dalle 18:00 alle 05:59), nel fine settimana (sabato e domenica) e nei giorni festivi. Calcolare la classifica delle prese intelligenti in base alla differenza dei consumi energetici medi mensili tra la fascia di punta e la fascia fuori punta. Nella classifica le prese sono ordinate in modo decrescente, riportando, come primi elementi, le prese che non sfruttano la fascia fuori punta.

# Getting Started ##


### Prerequiti ###

* Aver installato Apache Spark e Hadoop

### Developer ###

* Alberto Talone
* Andrea Cenciarelli
