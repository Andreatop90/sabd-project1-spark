package com.sabd.project1.util;

import com.sabd.project1.Global;
import scala.Tuple2;
import scala.Tuple3;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TimeZone;
import java.util.TreeSet;

public class Tools {

    public static Comparator<Tuple3<Float, Long, Boolean>> compare =
            (Tuple3<Float, Long, Boolean> o1, Tuple3<Float, Long, Boolean> o2) -> {

                if (o1._2() < o2._2()) {
                    return -1;
                } else if (o1._2().equals(o2._2())) {
                    return 0;
                } else
                    return 1;
            };

    public static LocalDateTime getDate(Long timestamp) {

        LocalDateTime triggerTime =
                LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
                        TimeZone.getDefault().toZoneId());
        return triggerTime;
    }


    public static Integer getTimeSlot(Long timestamp) {
        LocalDateTime triggerTime =
                LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp),
                        TimeZone.getDefault().toZoneId());

        if (triggerTime.getHour() < 6) {
            return 1;
        } else if (triggerTime.getHour() < 12) {
            return 2;
        } else if (triggerTime.getHour() < 18) {
            return 3;
        } else {
            return 4;
        }
    }


    public static boolean checkReset(TreeSet<Tuple3<Float, Long, Boolean>> input) {

        ArrayList<Tuple3<Float, Long, Boolean>> p = new ArrayList<>(input);
        for (int i = 0; i < input.size() - 1; i++) {
            if (p.get(i + 1)._2() > p.get(i)._2() && p.get(i + 1)._1() < p.get(i)._1() && (p.get(i)._1() - p.get(i + 1)._1()) > Global.TOLERANCE) {

                return true;
            }
        }
        return false;
    }


    public static Integer getFascia(Long timestamp) {

        int hour = Tools.getDate(timestamp).getHour();
        int day = Tools.getDate(timestamp).getDayOfWeek().getValue();

        if ((hour >= 6 && hour < 18) && (day >= DayOfWeek.MONDAY.getValue() && day <= DayOfWeek.FRIDAY.getValue())) {
            return 1;
        } else {
            return 2;
        }
    }

    public static Float getTotal(TreeSet<Tuple2<Float, Long>> tree) {

        ArrayList<Tuple2<Float, Long>> p = new ArrayList<>(tree);
        Float total = 0.0f;

        for (int i = 0; i < p.size() - 1; i++) {
            if (p.get(i + 1)._1() < p.get(i)._1()) {
                total += p.get(i + 1)._1();
            } else {
                total += (p.get(i + 1)._1() - p.get(i)._1());
            }
        }

        return total;
    }
}
