package com.sabd.project1.entity;

import java.io.Serializable;
import java.util.List;

public class Record implements Serializable {

    private Long id;
    private Long timestamp;
    private Float value;
    private Boolean property;
    private Integer plug_id;
    private Integer household_id;
    private Integer house_id;

    public Record(List record) {
        this.id = Long.parseLong(record.get(0).toString());
        this.timestamp = Long.parseLong(record.get(1).toString());
        this.value = Float.parseFloat(record.get(2).toString());
        if(Integer.parseInt(record.get(3).toString()) == 0){
            // Consume since first boot (last reboot)
            this.property = false;
        } else {
            // Instant consume
            this.property = true;
        }
        this.plug_id = Integer.parseInt(record.get(4).toString());
        this.household_id = Integer.parseInt(record.get(5).toString());
        this.house_id = Integer.parseInt(record.get(6).toString());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Boolean getProperty() {
        return property;
    }

    public void setProperty(Boolean property) {
        this.property = property;
    }

    public Integer getPlug_id() {
        return plug_id;
    }

    public void setPlug_id(Integer plug_id) {
        this.plug_id = plug_id;
    }

    public Integer getHousehold_id() {
        return household_id;
    }

    public void setHousehold_id(Integer household_id) {
        this.household_id = household_id;
    }

    public Integer getHouse_id() {
        return house_id;
    }

    public void setHouse_id(Integer house_id) {
        this.house_id = house_id;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", timestamp=" + timestamp +
                ", value=" + value +
                ", property=" + property +
                ", plug_id=" + plug_id +
                ", household_id=" + household_id +
                ", house_id=" + house_id +
                '}';
    }
}
