package com.sabd.project1.kafka;

import com.sabd.project1.Global;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

//import simple producer packages
//import KafkaProducer packages
//import ProducerRecord packages

//Create java class named “kafka.producer.SimpleProducer”
public class KafkaSender {

    public KafkaSender(){
        super();
    }

    private Properties createProperties(){

        Properties props = new Properties();

        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,Global.BROKERS);

        //Set acknowledgements for producer requests.
        props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", Global.NUM_RETRY_REQUEST);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory",33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        return props;
    }

    private Producer<String, String> getProducer() {
        return new KafkaProducer<>(createProperties());
    }

    public void sendMessageToTopic(String topicName, String value){

        Producer<String,String> producer = getProducer();
        producer.send(new ProducerRecord<>(topicName,value));
        producer.close();
    }
}