package com.sabd.project1.spark;

import com.google.gson.Gson;
import com.sabd.project1.Global;
import com.sabd.project1.entity.Record;
import com.sabd.project1.kafka.KafkaSender;
import com.sabd.project1.model.JsonOuput;
import com.sabd.project1.util.Tools;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;

import java.util.TreeSet;

public class Queries {


    public static void calculateQuery1(JavaRDD<Record> myRecord) {

        JavaRDD<Record> instantConsume = myRecord.filter(rec -> rec.getProperty());

        //Aggrego i dati per chiave composta da Id Casa e Timestamp, trattandosi di consumo istantaneo sommo
        // i cosumi delle prese appartenenti allo stesso istante
        JavaPairRDD<Tuple2<Integer, Long>, Float> groupedByID = instantConsume.mapToPair(
                rec -> (new Tuple2<>(new Tuple2<>(rec.getHouse_id(), rec.getTimestamp()), rec.getValue())));

        //Sommo i valori delle prese della singola casa nel singolo istante
        JavaPairRDD<Tuple2<Integer, Long>, Float> houses = groupedByID.reduceByKey((record, record2) -> record + record2);

        //Filtro le case che hanno sforato il tetto massimo di riferimento
        JavaPairRDD<Tuple2<Integer, Long>, Float> query1 = houses.filter(rec -> rec._2 >= 350.0).distinct();

        //Riporto il valore del solo ID delle case senza ripetizioni
        JavaRDD<Integer> resultQuery1 = query1.map(tuple2FloatTuple2 -> tuple2FloatTuple2._1._1).distinct();


        //Invio output su Kafka
        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery1.collect().toString();
        JsonOuput output = new JsonOuput("Query1", stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT, gson.toJson(output));
        System.out.println(gson.toJson(output));


    }

    public static void calculateQuery2(JavaRDD<Record> myRecord) {

        JavaRDD<Record> effectiveConsume = myRecord.filter(rec -> !rec.getProperty());

        //Eseguo il map inserendo un parametro booleano che verifichi un eventuale reset
        JavaPairRDD<Tuple4<Integer, Integer, Integer, Integer>, Tuple3<Float, Long, Boolean>> groupedByKeys = effectiveConsume.mapToPair(record ->
                new Tuple2<>(new Tuple4<>(record.getPlug_id(), record.getHouse_id(), Tools.getDate(record.getTimestamp()).getDayOfYear(), Tools.getTimeSlot(record.getTimestamp())),
                        new Tuple3<>(record.getValue(), record.getTimestamp(), false)));

        //Raggruppo i valori per chiave composita
        JavaPairRDD<Tuple4<Integer, Integer, Integer, Integer>, Iterable<Tuple3<Float, Long, Boolean>>> tupleMaxMin = groupedByKeys.groupByKey();

        //Scorro ogni iterable legato alla chiave restituendo una tupla con il consumo e una flag booleano che indica la presenza di un reset
        JavaPairRDD<Tuple4<Integer, Integer, Integer, Integer>, Tuple2<Float, Boolean>> maxMinCalculated = tupleMaxMin.mapToPair(input -> {

            TreeSet<Tuple3<Float, Long, Boolean>> tree = new TreeSet(Tools.compare);

            for (Tuple3<Float, Long, Boolean> t : input._2) {
                tree.add(t);
            }
            return new Tuple2<>(input._1(), new Tuple2<>(tree.last()._1() - tree.first()._1(), Tools.checkReset(tree)));
        });
        //Filtro scartando i valori in cui il flag è impostato a true
        JavaPairRDD<Tuple4<Integer, Integer, Integer, Integer>, Float> maxMinFiltered = maxMinCalculated.filter(input -> !input._2()._2())
                .mapToPair(input -> new Tuple2<>(input._1(), input._2()._1()));


        //Aggrego i dati sommando i valori delle prese
        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Float> aggregateByPlugID = maxMinFiltered.mapToPair(input -> new Tuple2<>(
                new Tuple3<>(input._1._2(), input._1._3(), input._1._4()), input._2())
        );

        //Sommo i risultati delle prese appartenenti alla stessa casa
        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Float> sumByPlugID = aggregateByPlugID.reduceByKey((aFloat, aFloat2) -> aFloat + aFloat2);

        //Eseguo il map per preparare il calcolo della media e della deviazione standard
        JavaPairRDD<Tuple2<Integer, Integer>, Tuple3<Float, Float, Float>> aggregateByDay = sumByPlugID.mapToPair(input -> new Tuple2<>(
                new Tuple2<>(input._1._1(), input._1._3()), new Tuple3<>(input._2(), (float) Math.pow(input._2, 2), 1.0f))
        );

        //Eseguo il reduce dei valori precedentemente mappati
        JavaPairRDD<Tuple2<Integer, Integer>, Tuple3<Float, Float, Float>> mediaVarianceHomeTimeslot = aggregateByDay.reduceByKey((input, input2) ->

                new Tuple3<>(input._1() + input2._1(), input._2() + input2._2(), input._3() + input2._3())
        );

        //Eseguo il Map in modo da avere in uscita una tupla che contenga per ogni chiave considerata media e varianza richieste
        JavaPairRDD<Tuple2<Integer, Integer>, Tuple2<Float, Float>> resultQuery2 = mediaVarianceHomeTimeslot.mapToPair(input ->
                new Tuple2<>(input._1, new Tuple2<>(input._2._1() / input._2._3(), (float) (Math.sqrt(input._2._2() / input._2._3() - Math.pow(input._2._1() / input._2._3(), 2))))));

        //Invio l'output su Kafka
        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery2.collect().toString();
        JsonOuput output = new JsonOuput("Query2", stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT, gson.toJson(output));
        System.out.println(gson.toJson(output));

    }

    public static void calculateQuery3(JavaRDD<Record> myRecord) {

        JavaRDD<Record> effectiveConsume = myRecord.filter(rec -> !rec.getProperty());

        JavaPairRDD<Tuple5<Integer, Integer, Integer, Integer, Integer>, Tuple3<Float, Long, Boolean>> groupedByKeys = effectiveConsume.mapToPair(record ->
                new Tuple2<>(new Tuple5<>(record.getHouse_id(), record.getPlug_id(), Tools.getDate(record.getTimestamp()).getMonthValue(), Tools.getDate(record.getTimestamp()).getDayOfYear(), Tools.getFascia(record.getTimestamp())),
                        new Tuple3<>(record.getValue(), record.getTimestamp(), false)));


        JavaPairRDD<Tuple5<Integer, Integer, Integer, Integer, Integer>, Iterable<Tuple3<Float, Long, Boolean>>> tupleMaxMin = groupedByKeys.groupByKey();

        JavaPairRDD<Tuple5<Integer, Integer, Integer, Integer, Integer>, Tuple2<Float, Boolean>> maxMinCalculated = tupleMaxMin.mapToPair(input -> {

            TreeSet<Tuple3<Float, Long, Boolean>> tree = new TreeSet(Tools.compare);

            for (Tuple3<Float, Long, Boolean> t : input._2) {
                tree.add(t);
            }
            return new Tuple2<>(input._1(), new Tuple2<>(tree.last()._1() - tree.first()._1(), Tools.checkReset(tree)));
        });

        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Tuple4<Float, Integer, Float, Integer>> newSum = maxMinCalculated.mapToPair(input -> {
            if (input._1._5() == 1) {
                return new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                        new Tuple4<>(input._2._1(), 1, 0.0f, 0));
            } else {
                return new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                        new Tuple4<>(0.0f, 0, input._2._1(), 1));
            }
        });

        // Tuple4<Valore sommato fascia di punta, contatore, valore sommato fascia fuori punta, contatore>
        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Tuple4<Float, Integer, Float, Integer>> totalSum = newSum.reduceByKey(
                (input1, input2) -> new Tuple4<>(input1._1() + input2._1(), input1._2() + input2._2(),
                        input1._3() + input2._3(), input1._4() + input2._4()));

        // Calcolo la media
        // Il risultato è una tuple2<Valore medio fascia di punta, Valore medio fuori fascia>
        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Tuple2<Float, Float>> avgTotal = totalSum.mapToPair(
                input -> new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                        new Tuple2<>(input._2._1() / input._2._2(),
                                input._2._3() / input._2._4()))
        );

        // Risultato finale
        // Effettuo la differenza tra il valore medio fascia di punta e il valore medio fuori fascia
        JavaPairRDD<Tuple3<Integer, Integer, Integer>, Float> invertQuery3 = avgTotal.mapToPair(
                input -> new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                        input._2._1() - input._2._2()));

        // Inverto chiave,valore per effettuare l'ordinamento desc sul valore.
        JavaPairRDD<Float, Tuple3<Integer, Integer, Integer>> resultQuery3 = invertQuery3.mapToPair(input -> input.swap()).sortByKey(false);

        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery3.collect().toString();
        JsonOuput output = new JsonOuput("Query3", stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT, gson.toJson(output));
        System.out.println(gson.toJson(output));


    }
}
