package com.sabd.project1.spark;


import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.sum;

public class QueriesSQL {


    public static void main(String[] args) {
        //Spark SQL
        SparkSession spark = SparkSession
                .builder()
                .appName("Project1")
                .config("spark.some.config.option", "some-value")
                .master("spark://andrea-XPS-15-9560:7077")
                .getOrCreate();
        Dataset<Row> dataset = spark.read().csv("alluxio://localhost:19998/input");
        calculateQuery1(spark, dataset);

    }

    private static void calculateQuery1(SparkSession spark, Dataset<Row> dataset) {

        Dataset<Row> filter1 = dataset.filter(col("_c3").equalTo(1));
        Dataset<Row> group1 = filter1.groupBy(col("_c1"), col("_c6")).agg(sum("_c2"));
        Dataset<Row> sum1 = group1.filter(col("sum(_c2)").gt(350));
        Dataset<Row> result = sum1.select("_c6").distinct();
        result.show();
    }

}
