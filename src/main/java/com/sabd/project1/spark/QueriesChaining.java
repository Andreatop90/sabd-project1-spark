package com.sabd.project1.spark;

import com.google.gson.Gson;
import com.sabd.project1.Global;
import com.sabd.project1.entity.Record;
import com.sabd.project1.kafka.KafkaSender;
import com.sabd.project1.model.JsonOuput;
import com.sabd.project1.util.Tools;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.Tuple5;
import java.util.TreeSet;

public class QueriesChaining {

    public static void calculateQuery1(JavaRDD<Record> myRecord){

        JavaRDD<Integer> resultQuery1 = myRecord.filter(rec -> rec.getProperty())
                .mapToPair(rec ->(new Tuple2<>(new Tuple2<>(rec.getHouse_id(),rec.getTimestamp()),rec.getValue())))
                .reduceByKey((record, record2) -> record+record2)
                .filter(rec -> rec._2 >= 350.0).distinct()
                .map(tuple2FloatTuple2 -> tuple2FloatTuple2._1._1).distinct();
        resultQuery1.collect();

        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery1.collect().toString();
        JsonOuput output = new JsonOuput("Query1",stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT,gson.toJson(output));
        System.out.println(gson.toJson(output));


    }

    public static void  calculateQuery2(JavaRDD<Record> myRecord){

        JavaPairRDD<Tuple2<Integer,Integer>,Tuple2<Float,Float>> resultQuery2 = myRecord.filter(rec -> !rec.getProperty())
                .mapToPair(record ->
                new Tuple2<>(new Tuple4<>(record.getPlug_id(),record.getHouse_id(), Tools.getDate(record.getTimestamp()).getDayOfYear(),Tools.getTimeSlot(record.getTimestamp())),
                        new Tuple3<>(record.getValue(),record.getTimestamp(),false)))
                .groupByKey()
                .mapToPair(input ->{
            TreeSet<Tuple3<Float,Long,Boolean>> tree = new TreeSet(Tools.compare);

            for (Tuple3<Float,Long,Boolean> t: input._2) {
                tree.add(t);
            }
            return new Tuple2<>(input._1(),new Tuple2<>(tree.last()._1()-tree.first()._1(),Tools.checkReset(tree)));
        }).filter(input -> !input._2()._2())
                .mapToPair(input -> new Tuple2<>(input._1(),input._2()._1()))
                .mapToPair(input -> new Tuple2<>(
                new Tuple3<>(input._1._2(),input._1._3(),input._1._4()),input._2())
                )
                .reduceByKey((aFloat, aFloat2) -> aFloat+aFloat2)

        .mapToPair(input -> new Tuple2<>(
                new Tuple2<>(input._1._1(),input._1._3()), new Tuple3<>(input._2(),(float)Math.pow(input._2,2),1.0f))
        )

        .reduceByKey((input, input2) ->

                new Tuple3<>(input._1()+input2._1(),input._2()+input2._2(),input._3()+input2._3())
        )

        .mapToPair(input ->
                new Tuple2<>(input._1,new Tuple2<>(input._2._1()/input._2._3(),(float)(Math.sqrt(input._2._2()/input._2._3() - Math.pow(input._2._1()/input._2._3(),2))))));

        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery2.collect().toString();
        JsonOuput output = new JsonOuput("Query2",stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT,gson.toJson(output));
        System.out.println(gson.toJson(output));


    }

    public static void calculateQuery3(JavaRDD<Record> myRecord){

        JavaPairRDD<Float, Tuple3<Integer, Integer, Integer>> resultQuery3 = myRecord.filter(rec -> !rec.getProperty())
                .mapToPair(record -> new Tuple2<>(new Tuple5<>(record.getHouse_id(), record.getPlug_id(), Tools.getDate(record.getTimestamp()).getMonthValue(),
                                            Tools.getDate(record.getTimestamp()).getDayOfYear(), Tools.getFascia(record.getTimestamp())),
                                new Tuple3<>(record.getValue(),record.getTimestamp(), false)))
                .groupByKey()
                .mapToPair(input ->{
                    TreeSet<Tuple3<Float,Long,Boolean>> tree = new TreeSet(Tools.compare);

                    for (Tuple3<Float,Long,Boolean> t: input._2) {
                        tree.add(t);
                    }
                    return new Tuple2<>(input._1(),new Tuple2<>(tree.last()._1()-tree.first()._1(),Tools.checkReset(tree)));})
                .filter(input -> !input._2()._2())
                .mapToPair(input -> {
                    if(input._1._5() == 1){
                        return new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                                new Tuple4<>(input._2._1(), 1, 0.0f, 0));
                    } else {
                        return new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                                new Tuple4<>(0.0f, 0, input._2._1(), 1));
                    }})
                .reduceByKey(
                        (input1, input2) -> new Tuple4<>(input1._1() + input2._1(), input1._2() + input2._2(),
                                input1._3() + input2._3(), input1._4() + input2._4()))
                .mapToPair(input -> new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                                new Tuple2<>(input._2._1()/input._2._2(),
                                        input._2._3()/ input._2._4())))
                .mapToPair(input -> new Tuple2<>(new Tuple3<>(input._1._1(), input._1._2(), input._1._3()),
                                input._2._1() - input._2._2()))
                .mapToPair(input -> input.swap()).sortByKey(false);

        KafkaSender send = new KafkaSender();
        String stringOutput = resultQuery3 .collect().toString();
        JsonOuput output = new JsonOuput("Query3",stringOutput);

        Gson gson = new Gson();
        send.sendMessageToTopic(Global.TOPIC_OUTPUT,gson.toJson(output));
        System.out.println(gson.toJson(output));

    }
}
