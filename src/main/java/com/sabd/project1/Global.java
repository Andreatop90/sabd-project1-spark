package com.sabd.project1;


public class Global {

    public static int NUM_RETRY_REQUEST = 0;
    public static String ZOOKEEPER = "localhost:2181";
    public static String BROKERS = "localhost:9092";
    public static Integer VALIDE_RECORD = 7;
    public static String TOPIC_OUTPUT = "output";
    public static double TOLERANCE = 2;

}
