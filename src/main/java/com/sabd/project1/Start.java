package com.sabd.project1;

import com.sabd.project1.entity.Record;
import com.sabd.project1.spark.Queries;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Start {

    private static final Pattern NEWLINE = Pattern.compile("\n");
    private static final Pattern COMMA = Pattern.compile(",");

    public static void main(String[] args) {

        SparkConf myConf = new SparkConf().setAppName("Project1").setMaster("spark://andrea-XPS-15-9560:7077")
                .set("spark.executor.memory", "4g");

        JavaSparkContext myContext = new JavaSparkContext(myConf);

        //DATA INGESTION
        JavaRDD<String> lines = myContext.textFile("alluxio://localhost:19998/input");

        //DATA INITIAL MANIPULATION

        // Map dell'oggetto in righe
        JavaRDD<String> line = lines.flatMap(s -> Arrays.asList(NEWLINE.split(s)).iterator());

        // Filtro che esclude stringhe malformate
        JavaRDD<String> validRecord = line.filter(s -> Arrays.asList(COMMA.split(s)).size() == Global.VALIDE_RECORD);

        // Creazione dell'oggetto record
        JavaRDD<Record> myRecord = validRecord.map(s -> {
            List record = Arrays.asList(COMMA.split(s));
            return new Record(record);
        }).cache();

        Queries.calculateQuery1(myRecord);
        Queries.calculateQuery2(myRecord);
        Queries.calculateQuery3(myRecord);

        myContext.close();
    }
}
